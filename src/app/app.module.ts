import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NbThemeModule, 
  NbLayoutModule , 
  NbIconModule , 
  NbActionsModule,
  NbSidebarModule, 
  NbButtonModule,
  NbCardModule,
  NbMenuModule,
  NbWindowModule,
  NbInputModule,
  NbSelectModule,
  NbRadioModule} from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { BoardComponent } from './components/board/board.component';
import { GetInputComponent } from './components/get-input/get-input.component';
import { ChartsModule } from 'ng2-charts';
import { ChainaComponent } from './components/chaina/chaina.component';
import { UsaComponent } from './components/usa/usa.component';
import { JapanComponent } from './components/japan/japan.component';
import { MalaysiaComponent } from './components/malaysia/malaysia.component';
import { SingaporeComponent } from './components/singapore/singapore.component';
import { HomeComponent } from './components/home/home.component';



@NgModule({
  declarations: [
    AppComponent,
    BoardComponent,
    GetInputComponent,
    ChainaComponent,
    UsaComponent,
    JapanComponent,
    MalaysiaComponent,
    SingaporeComponent,
    HomeComponent,
  ],
  imports: [
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NbThemeModule.forRoot({ name: 'dark' }),
    NbLayoutModule,
    NbEvaIconsModule,
    NbIconModule,
    NbActionsModule,
    NbSidebarModule.forRoot(), 
    NbButtonModule,
    NbCardModule,
    NbMenuModule.forRoot(),
    NbWindowModule,
    NbInputModule,
    NbSelectModule,
    NbRadioModule,
    ChartsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
