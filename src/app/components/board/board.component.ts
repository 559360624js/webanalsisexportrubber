import { Component, OnInit } from '@angular/core';
import {Chart} from 'chart.js';
// import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
// import { Color } from 'ng2-charts';
// import * as XLSX from 'xlsx';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss']
})
export class BoardComponent implements OnInit {

    private option; 
    public oilpice:number;
    public rubber:number;
    public amount:number;

    // public bubbleChartOptions: ChartOptions = {
    //     responsive: true,
    //     scales: {
    //       xAxes: [{
    //         ticks: {
    //           min: 0,
    //           max: 100,
    //         }
    //       }],
    //       yAxes: [{
    //         ticks: {
    //           min: 0,
    //           max: 100,
    //         }
    //       }]
    //     }
    //   };
    //   public bubbleChartType: ChartType = 'bubble';
    //   public bubbleChartLegend = true;
    
    //   public bubbleChartData: ChartDataSets[] = [
    //     {
    //       data: [
    //         { x: 10, y: 80, r: 10},
    //         { x: 15, y: 5, r: 10 },
    //         { x: 26, y: 12, r: 10 },
    //         { x: 7, y: 8, r: 10 },
    //       ],
    //       label: 'Series A',
    //     },
    //   ];

  constructor() { 

  }

  ngOnInit() {
        var ctx = <HTMLCanvasElement> document.getElementById('myChart');
        var myChart = new Chart(ctx, {
        type: 'line',
        data: {        
            datasets: [{
                label: 'Line Dataset',
                data: [0, 10, 20, 30, 40 , 50],
                backgroundColor: [
               'rgba(255, 99, 132, 0.0)',
            ],
                borderColor: [
                'rgba(75, 192, 192, 1)',
            ],          
             borderWidth: 2
            },{
                label: 'Bubble Dataset',
                data: [
                    { x: 10, y: 10, r: 10 },
                    { x: 15, y: 5, r: 10 },
                    { x: 26, y: 12, r: 10 },
                    { x: 7, y: 8, r: 10 },
                ],
                type: 'bubble',
                backgroundColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(255, 99, 132, 1)',
                    'rgba(255, 99, 132, 1)',
                    'rgba(255, 99, 132, 1)',

                 ],
                 borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(255, 99, 132, 1)',
                    'rgba(255, 99, 132, 1)',
                    'rgba(255, 99, 132, 1)',

                ],          
                
            } 
        ],
            labels: ['2015', '2016', '2017', '2018', '2019', '2020'],
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
 }



}


// Line chart เส้นหลายอัน
// datasets: [{
//     label: 'Chaina',
//     data: [10, 100, 3, 5, 2, 3],
//     backgroundColor: [
//         'rgba(255, 99, 132, 0.2)',
//     ],
//     borderColor: [
//         'rgba(255, 99, 132, 1)',
//         'rgba(54, 162, 235, 1)',
//         'rgba(255, 206, 86, 1)',
//         'rgba(75, 192, 192, 1)',
//         'rgba(153, 102, 255, 1)',
//         'rgba(255, 159, 64, 1)'
//     ],          
//     borderWidth: 3
// },
// {
//     label: 'USA',
//     data: [5, 20, 90, 100, 102, 130],
//     backgroundColor: [
//         'rgba(54, 162, 235, 0.2)',
//         'rgba(255, 206, 86, 0.2)',
//         'rgba(75, 192, 192, 0.2)',
//         'rgba(153, 102, 255, 0.2)',
//         'rgba(255, 159, 64, 0.2)'
//     ],
//     borderColor: [
//         'rgba(54, 162, 235, 1)',
//         'rgba(54, 162, 235, 1)',
//         'rgba(255, 206, 86, 1)',
//         'rgba(75, 192, 192, 1)',
//         'rgba(153, 102, 255, 1)',
//         'rgba(255, 159, 64, 1)'
//     ],          
//     borderWidth:3
// },
// {
//     label: 'Japan',
//     data: [50, 20, 30, 80, 20, 40,60],
//     backgroundColor: [
//         'rgba(255, 206, 86, 0.2)',
//         'rgba(75, 192, 192, 0.2)',
//         'rgba(153, 102, 255, 0.2)',
//         'rgba(255, 159, 64, 0.2)'
//     ],
//     borderColor: [
//         'rgba(255, 206, 86, 1)',
//         'rgba(54, 162, 235, 1)',
//         'rgba(255, 206, 86, 1)',
//         'rgba(75, 192, 192, 1)',
//         'rgba(153, 102, 255, 1)',
//         'rgba(255, 159, 64, 1)'
//     ],          
//     borderWidth:3
// },
// {
//     label: 'Malaysia',
//     data: [70, 20, 10, 80, 40, 90,20],
//     backgroundColor: [
//         'rgba(153, 102, 255, 0.2)',
//         'rgba(255, 159, 64, 0.2)'
//     ],
//     borderColor: [
//         'rgba(153, 102, 255, 1)',
//         'rgba(54, 162, 235, 1)',
//         'rgba(255, 206, 86, 1)',
//         'rgba(75, 192, 192, 1)',
//         'rgba(153, 102, 255, 1)',
//         'rgba(255, 159, 64, 1)'
//     ],          
//     borderWidth:3
// },
// ],


//### 2 ###### อันนี้สองอัน
    //     var ctx = document.getElementById('myChart');
    //     var myChart = new Chart(ctx, {
    //     type: 'line',
    //     data: {        
    //         datasets: [{
    //             label: 'Line Dataset',
    //             data: [0, 10, 20, 30, 40 , 50],
    //             backgroundColor: [
    //            'rgba(255, 99, 132, 0.0)',
    //         ],
    //             borderColor: [
    //             'rgba(75, 192, 192, 1)',
    //         ],          
    //          borderWidth: 2
    //         },{
    //             label: 'Bubble Dataset',
    //             data: [
    //                 { x: 10, y: 10, r: 10 },
    //                 { x: 15, y: 5, r: 10 },
    //                 { x: 26, y: 12, r: 10 },
    //                 { x: 7, y: 8, r: 10 },
    //             ],
    //             type: 'bubble',
    //             backgroundColor: [
    //                 'rgba(255, 99, 132, 1)',
    //                 'rgba(255, 99, 132, 1)',
    //                 'rgba(255, 99, 132, 1)',
    //                 'rgba(255, 99, 132, 1)',

    //              ],
    //              borderColor: [
    //                 'rgba(255, 99, 132, 1)',
    //                 'rgba(255, 99, 132, 1)',
    //                 'rgba(255, 99, 132, 1)',
    //                 'rgba(255, 99, 132, 1)',

    //             ],          
                
    //         } 
    //     ],
    //         labels: ['2015', '2016', '2017', '2018', '2019', '2020'],
    //     },
    //     options: {
    //         scales: {
    //             yAxes: [{
    //                 ticks: {
    //                     beginAtZero: true
    //                 }
    //             }]
    //         }
    //     }
    // });
