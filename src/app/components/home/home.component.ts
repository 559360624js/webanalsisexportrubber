import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import ratedata1 from '../../../assets/ch.json';
import ratedata from '../../../assets/jpn.json';
import oildata from '../../../assets/priceoil.json';
import rubberdata from '../../../assets/rubber.json';
import exportdata_jpn from '../../../assets/export_jpn.json';
import exportdata_ch from '../../../assets/export_ch.json';
import exportdata_usa from '../../../assets/export_usa.json';
import exportdata_mls from '../../../assets/export_mls.json';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public amount:number;  
  year_v:string;
  dataPie_v:number[] = [59.7667191,0.873399276,9.828419772,23.62900156];

  rate_data:number[] = this.rate();
  rate_year:string[] = this.yaerrate();

  oil_data:number[] = this.oil();

  rubber_data:number[] = this.rubber_value();

  export_jpn:number[] = this.export_value_jpn();

  export_ch:number[] = this.export_value_ch();

  export_usa:number[] = this.export_value_usa();

  export_mls:number[] = this.export_value_mls();

  chartData: string[];
  chartLabels: string[];
  chartColors: string[];
  
  constructor() { 

  }

  ngOnInit() {
    this.lineChart1();
    this.lineChart2();
    this.test2();
  }
  
  test(){
    this.test2();
    this.ngOnInit();
  }

  test2(){
    var year = this.year_v;

    switch (year) {
        case "2014":
          this.dataPie_v = [59.7667191,0.873399276,9.828419772,23.62900156];
          return this.dataPie_v
        case "2015":
          this.dataPie_v = [63.4493694,0.746044278,8.226134689,21.69864275];
          return this.dataPie_v
        case "2016":
          this.dataPie_v = [58.6132102,0.591301795,7.70383872 ,25.8859395];
          return this.dataPie_v
        case "2017":
          this.dataPie_v = [62.61282252,0.246363291,7.870915995,22.61244089];
          return this.dataPie_v
        case "2018":
          this.dataPie_v = [58.44327054,0.047188877,8.364982376,25.57955527];
          return this.dataPie_v
        default:
          this.year_v = "2014";
          break;
    } 
  }


  lineChart1(){

    var ctx = <HTMLCanvasElement> document.getElementById('myChart');
    var myChart = new Chart(ctx,{
      type: 'pie',
      data: {        
        datasets: [{
            label: 'ราคาน้ำมัน',
            data: this.dataPie_v,
            backgroundColor: [
                'rgba(255, 99, 132, 0.5)',
                'rgba(54, 162, 235, 0.5)',
                'rgba(0, 214, 143, 0.5)',
                'rgba(255, 206, 86, 0.5)',
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(0, 214, 143, 1)',
                'rgba(255, 206, 86, 1)'
            ],          
            borderWidth: 3
        },
        ],
              labels: ["จีน","อเมริกา","ญี่ปุ่น","มาเลเซีย"],
          },
      options: {
          scales: {
          }
      }
    });

  
  }

  lineChart2(){
        var ctx = <HTMLCanvasElement> document.getElementById('myChart2');
        var myChart = new Chart(ctx, {
        type: 'line',
        data: {        
datasets: [{
    label: 'จีน',
    data: this.export_ch,
    fill: false,
    lineTension: 0.2,
    borderColor: 'rgba(255, 99, 132, 1)', // สีของเส้น       
    borderWidth: 1.5
},
//'rgba(255, 206, 86, 1)'
{
    label: 'อเมริกา',
    data: this.export_usa,
    fill: false,
    lineTension: 0.2,
    borderColor: 'rgba(54, 162, 235, 1)', // สีของเส้น       
    borderWidth: 1.5
},
{
    label: 'ญี่ปุ่น',
    data: this.export_jpn,
    fill: false,
    lineTension: 0.2,
    borderColor: 'rgba(0, 214, 143, 1)', // สีของเส้น       
    borderWidth: 1.5
},
{
    label: 'มาเลเซีย',
    data: this.export_mls,
    fill: false,
    lineTension: 0.2,
    borderColor: 'rgba(255, 206, 86, 1)', // สีของเส้น       
    borderWidth: 1.5
},
],
            labels: this.rate_year,
        },
        options: {
            scales: {
              xAxes: [{
                display: true,
                scaleLabel: {
                  display: true,
                  labelString: 'Month / Year'
                }
              }],
              yAxes: [{
                display: true,
                scaleLabel: {
                  display: true,
                  labelString: 'Baht'
                }
              }]
            }
        }
    });
    

  
  }


  rate(){
    let obj = ratedata;
    let op = obj.dataPoints.map(({average}) => average )
    return op ;
  }

  yaerrate(){
    let obj = ratedata1;
    let year_rate = obj.dataPoints.map(({month}) => month )
    return year_rate;
  }

  oil(){
    let obj = oildata;
    let pice_oil = obj.dataPoints.map(({priceoil}) => priceoil )
    return pice_oil;
  }

  rubber_value(){
    let obj = rubberdata;
    let many_rubber = obj.dataPoints.map(({rubber}) => rubber )
    return many_rubber;
  }

  export_value_jpn(){
    let obj = exportdata_jpn;
    let many_export = obj.dataPoints.map(({export_v}) => export_v )
    return many_export;
  }

  export_value_ch(){
    let obj = exportdata_ch;
    let many_export = obj.dataPoints.map(({export_ch}) => export_ch )
    return many_export;
  }

  export_value_usa(){
    let obj = exportdata_usa;
    let many_export = obj.dataPoints.map(({export_v}) => export_v )
    return many_export;
  }

  export_value_mls(){
    let obj = exportdata_mls;
    let many_export = obj.dataPoints.map(({export_v}) => export_v )
    return many_export;
  }

}
