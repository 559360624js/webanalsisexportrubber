import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
// import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import ratedata from '../../../assets/ch.json';
import oildata from '../../../assets/priceoil.json';
import rubberdata from '../../../assets/rubber.json';
import exportdata from '../../../assets/export_jpn.json';

// อัตราแลกเปลี่ยน
import rate_ch_2014 from '../../../assets/rate_jp/rate_japan_2014.json';
import rate_ch_2015 from '../../../assets/rate_jp/rate_japan_2015.json';
import rate_ch_2016 from '../../../assets/rate_jp/rate_japan_2016.json';
import rate_ch_2017 from '../../../assets/rate_jp/rate_japan_2017.json';
import rate_ch_2018 from '../../../assets/rate_jp/rate_japan_2018.json';

//ปริมาณยางพาราหลัก
import rubber_2014 from '../../../assets/rubber_value/rubber_2014.json';
import rubber_2015 from '../../../assets/rubber_value/rubber_2015.json';
import rubber_2016 from '../../../assets/rubber_value/rubber_2016.json';
import rubber_2017 from '../../../assets/rubber_value/rubber_2017.json';
import rubber_2018 from '../../../assets/rubber_value/rubber_2018.json';

// อัตราการการส่งออก
import export_2014 from '../../../assets/export_jp/export_japan_2014.json';
import export_2015 from '../../../assets/export_jp/export_japan_2015.json';
import export_2016 from '../../../assets/export_jp/export_japan_2016.json';
import export_2017 from '../../../assets/export_jp/export_japan_2017.json';
import export_2018 from '../../../assets/export_jp/export_japan_2018.json';

// ราคาน้ำมัน
import oil_2014 from '../../../assets/oil_value/price_oil_2014.json';
import oil_2015 from '../../../assets/oil_value/price_oil_2015.json';
import oil_2016 from '../../../assets/oil_value/price_oil_2016.json';
import oil_2017 from '../../../assets/oil_value/price_oil_2017.json';
import oil_2018 from '../../../assets/oil_value/price_oil_2018.json';

@Component({
  selector: 'app-japan',
  templateUrl: './japan.component.html',
  styleUrls: ['./japan.component.scss']
})
export class JapanComponent implements OnInit {

  rate_data:number[] = this.rate();
  rate_year:string[] = this.yaerrate();

  oil_data:number[] = this.oil();

  rubber_data:number[] = this.rubber_value();

  export_data:number[] = this.export_value();

  //ค่าที่ใช้ในเงื่อนไขตัวเลือกปี
  year_v:string = "2014";

  //เดือน
  month_v:string[] = this.month();


  // ค่าอัตราแลกเปลี่ยน
  rch2014:number[] = this.rate_jp_2014();
  rch2015:number[] = this.rate_jp_2015();
  rch2016:number[] = this.rate_jp_2016();
  rch2017:number[] = this.rate_jp_2017();
  rch2018:number[] = this.rate_jp_2018();
  //รวมค่าอัตราแลกเปลี่ยน
  rch_a:number[] = this.case_ch();

  // ปริมาณยางพารา
  rubber2014:number[] = this.rubber_home_2014();
  rubber2015:number[] = this.rubber_home_2015();
  rubber2016:number[] = this.rubber_home_2016();
  rubber2017:number[] = this.rubber_home_2017();
  rubber2018:number[] = this.rubber_home_2018();
  //ตัวแปรรวมยางพารา
  rb_a:number[] = this.case_rubber();

  //ปริมาณการส่งออก
  ex_2014:number[] = this.export_V_2014();
  ex_2015:number[] = this.export_V_2015();
  ex_2016:number[] = this.export_V_2016();
  ex_2017:number[] = this.export_V_2017();
  ex_2018:number[] = this.export_V_2018();
  //ตัวแปรรวมการส่งออก
  ex_a:number[] = this.case_ex();

  //ราคาน้ำมัน
  o_2014:number[] = this.oil_V_2014();
  o_2015:number[] = this.oil_V_2015();
  o_2016:number[] = this.oil_V_2016();
  o_2017:number[] = this.oil_V_2017();
  o_2018:number[] = this.oil_V_2018();
  //ตัวแปรรวม
  o_a:number[] = this.case_oil();



  
  constructor() { }

  ngOnInit() {
    this.lineChart1();
    this.lineChart2();
    this.lineChart3();
    this.lineChart4();

  }
  

  lineChart1(){
    var ctx = <HTMLCanvasElement> document.getElementById('myChart');
    var myChart = new Chart(ctx,{
      type: 'line',
      data: {        
        datasets: [{
            label: 'ราคาน้ำมัน',
            data: this.o_a,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)'
            ],
            borderColor: 'rgba(255, 99, 132, 1)',        
            borderWidth: 1.5
        },
        ],
              labels: this.month_v,
          },
      options: {
        scales: {
          xAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'เดือน / ปี'
            }
          }],
          yAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'บาท'
            }
          }]
        }
      }
    });
  
  }

  lineChart2(){
    var ctx = <HTMLCanvasElement> document.getElementById('myChart2');
    var myChart = new Chart(ctx,{
      type: 'line',
      data: {        
        datasets: [{
            label: 'ปริมาณยาง',
            data: this.rb_a,
            backgroundColor: [
                'rgba(0, 214, 143, 0.2)'
            ],
            borderColor: 'rgba(0, 214, 143, 1)',          
            borderWidth: 1.5
        },
        ],
              labels: this.month_v,
          },
      options: {
        scales: {
          xAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'เดือน / ปี'
            }
          }],
          yAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'หมื่นตัน'
            }
          }]
        }
      }
    });
  
  }

  lineChart3(){
    var ctx = <HTMLCanvasElement> document.getElementById('myChart3');
    var myChart = new Chart(ctx,{
      type: 'line',
      data: {        
        datasets: [{
            label: 'อัตราการแลกเปลี่ยนเงิน',
            data: this.rch_a,
            backgroundColor: [
                'rgba(54, 162, 235, 0.2)',
            ],
            borderColor: 'rgba(54, 162, 235, 1)',          
            borderWidth: 1.5
        },
        ],
              labels: this.month_v,
          },
      options: {
        scales: {
          xAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'เดือน / ปี'
            }
          }],
          yAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'บาท'
            }
          }]
        }
      }
    });
  
  }

  lineChart4(){
    var ctx = <HTMLCanvasElement> document.getElementById('myChart4');
    var myChart = new Chart(ctx,{
      type: 'line',
      data: {        
        datasets: [{
            label: 'ปริมาณการส่งออก',
            data: this.ex_a,
            backgroundColor: [
                'rgba(255, 206, 86, 0.2)',
            ],
            borderColor: 'rgba(255, 206, 86, 1)',   
            borderWidth: 1.5
        },
        ],
              labels: this.month_v ,
          },
      options: {
        scales: {
          xAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'เดือน / ปี'
            }
          }],
          yAxes: [{
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'กิโลกรัม'
            }
          }]
        }
      }
    });
  
  }

  rate(){
    let obj = ratedata;
    let op = obj.dataPoints.map(({average}) => average )
    return op ;
  }

  // เดือน
  month(){
    let obj = rate_ch_2014;
    let m = obj.dataPoints.map(({ month }) => month )
    return m ;
  }



  // ปริมาณยางพารา
  rubber_home_2014(){
    let obj = rubber_2014;
    let ru = obj.dataPoints.map(({ rubber }) => rubber )
    return ru ;
  }
  rubber_home_2015(){
    let obj = rubber_2015;
    let ru = obj.dataPoints.map(({ rubber }) => rubber )
    return ru ;
  }
  rubber_home_2016(){
    let obj = rubber_2016;
    let ru = obj.dataPoints.map(({ rubber }) => rubber )
    return ru ;
  }
  rubber_home_2017(){
    let obj = rubber_2017;
    let ru = obj.dataPoints.map(({ rubber }) => rubber )
    return ru ;
  }
  rubber_home_2018(){
    let obj = rubber_2018;
    let ru = obj.dataPoints.map(({ rubber }) => rubber )
    return ru ;
  }
  
  //เลือกค่ายางพารา
  case_rubber(){
    var year = this.year_v;

    switch (year) {
        case "2014":

          return this.rubber2014
        case "2015":

          return this.rubber2015
        case "2016":

          return this.rubber2016
        case "2017":

          return this.rubber2017
        case "2018":

          return this.rubber2018
        default:
          this.year_v = "2014";
          break;
    } 
  }
  testbtn(){
    this.rch_a = this.case_ch();
    this.rb_a = this.case_rubber();
    this.ex_a = this.case_ex();
    this.o_a= this.case_oil();
    this.ngOnInit();
    
  }
  // อัตราแลกเปลี่ยน
  rate_jp_2014(){
    let obj = rate_ch_2014;
    let r = obj.dataPoints.map(({ rate }) => rate )
    return r ;
  }
  rate_jp_2015(){
    let obj = rate_ch_2015;
    let r = obj.dataPoints.map(({ rate }) => rate )
    return r ;
  }
  rate_jp_2016(){
    let obj = rate_ch_2016;
    let r = obj.dataPoints.map(({ rate }) => rate )
    return r ;
  }
  rate_jp_2017(){
    let obj = rate_ch_2017;
    let r = obj.dataPoints.map(({ rate }) => rate )
    return r ;
  }
  rate_jp_2018(){
    let obj = rate_ch_2018;
    let r = obj.dataPoints.map(({ rate }) => rate )
    return r ;
  }

  // เลือกสำหรับอัตราแลกเปลี่ยน
  case_ch(){
    var year = this.year_v;

    switch (year) {
        case "2014":

          return this.rch2014
        case "2015":

          return this.rch2015
        case "2016":

          return this.rch2016
        case "2017":

          return this.rch2017
        case "2018":

          return this.rch2018
        default:
          this.year_v = "2014";
          break;
    } 
  }

  //ปริมาณการส่งออก
  export_V_2014(){
    let obj = export_2014;
    let r = obj.dataPoints.map(({ ex }) => ex )
    return r ;
  }
  export_V_2015(){
    let obj = export_2015;
    let r = obj.dataPoints.map(({ ex }) => ex )
    return r ;
  }
  export_V_2016(){
    let obj = export_2016;
    let r = obj.dataPoints.map(({ ex }) => ex )
    return r ;
  }
  export_V_2017(){
    let obj = export_2017;
    let r = obj.dataPoints.map(({ ex }) => ex )
    return r ;
  }
  export_V_2018(){
    let obj = export_2018;
    let r = obj.dataPoints.map(({ ex }) => ex )
    return r ;
  }

  // เลือกสำหรับอัตราแลกเปลี่ยน
  case_ex(){
    var year = this.year_v;

    switch (year) {
        case "2014":

          return this.ex_2014
        case "2015":

          return this.ex_2015
        case "2016":

          return this.ex_2016
        case "2017":

          return this.ex_2017
        case "2018":

          return this.ex_2018
        default:
          this.year_v = "2014";
          break;
    } 
  }

  //ปริมาณการส่งออก
  oil_V_2014(){
    let obj = oil_2014;
    let r = obj.dataPoints.map(({ price_oil }) => price_oil )
    return r ;
  }
  oil_V_2015(){
    let obj = oil_2015;
    let r = obj.dataPoints.map(({ price_oil }) => price_oil )
    return r ;
  }
  oil_V_2016(){
    let obj = oil_2016;
    let r = obj.dataPoints.map(({ price_oil }) => price_oil )
    return r ;
  }
  oil_V_2017(){
    let obj = oil_2017;
    let r = obj.dataPoints.map(({ price_oil }) => price_oil )
    return r ;
  }
  oil_V_2018(){
    let obj = oil_2018;
    let r = obj.dataPoints.map(({ price_oil }) => price_oil )
    return r ;
  }

  // เลือกสำหรับอัตราแลกเปลี่ยน
  case_oil(){
    var year = this.year_v;

    switch (year) {
        case "2014":

          return this.o_2014
        case "2015":

          return this.o_2015
        case "2016":

          return this.o_2016
        case "2017":

          return this.o_2017
        case "2018":

          return this.o_2018
        default:
          this.year_v = "2014";
          break;
    } 
  }








  

  yaerrate(){
    let obj = ratedata;
    let year_rate = obj.dataPoints.map(({month}) => month )
    return year_rate;
  }

  oil(){
    let obj = oildata;
    let pice_oil = obj.dataPoints.map(({priceoil}) => priceoil )
    return pice_oil;
  }

  rubber_value(){
    let obj = rubberdata;
    let many_rubber = obj.dataPoints.map(({rubber}) => rubber )
    return many_rubber;
  }

  export_value(){
    let obj = exportdata;
    let many_export = obj.dataPoints.map(({export_jp}) => export_jp )
    return many_export;
  }
  }