import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChainaComponent } from './chaina.component';

describe('ChainaComponent', () => {
  let component: ChainaComponent;
  let fixture: ComponentFixture<ChainaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChainaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChainaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
