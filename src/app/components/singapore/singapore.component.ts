import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-singapore',
  templateUrl: './singapore.component.html',
  styleUrls: ['./singapore.component.scss']
})
export class SingaporeComponent implements OnInit {

  datatest:number[]=[0,20,30,50,40,30];

  constructor() { }

  ngOnInit() {
    this.lineChart1();
    this.lineChart2();
    this.lineChart3();
    this.lineChart4();

  }

  lineChart1(){
    var ctx = <HTMLCanvasElement> document.getElementById('myChart');
    var myChart = new Chart(ctx,{
      type: 'line',
      data: {        
        datasets: [{
            label: 'Chaina',
            data: this.datatest,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],          
            borderWidth: 3
        },
        ],
              labels: ['2015', '2016', '2017', '2018', '2019', '2020'],
          },
      options: {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero: false
                  }
              }]
          }
      }
    });
  
  }

  lineChart2(){
    var ctx = <HTMLCanvasElement> document.getElementById('myChart2');
    var myChart = new Chart(ctx,{
      type: 'line',
      data: {        
        datasets: [{
            label: 'Chaina',
            data: this.datatest,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],          
            borderWidth: 3
        },
        ],
              labels: ['2015', '2016', '2017', '2018', '2019', '2020'],
          },
      options: {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero: false
                  }
              }]
          }
      }
    });
  
  }

  lineChart3(){
    var ctx = <HTMLCanvasElement> document.getElementById('myChart3');
    var myChart = new Chart(ctx,{
      type: 'line',
      data: {        
        datasets: [{
            label: 'Chaina',
            data: this.datatest,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],          
            borderWidth: 3
        },
        ],
              labels: ['2015', '2016', '2017', '2018', '2019', '2020'],
          },
      options: {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero: false
                  }
              }]
          }
      }
    });
  
  }

  lineChart4(){
    var ctx = <HTMLCanvasElement> document.getElementById('myChart4');
    var myChart = new Chart(ctx,{
      type: 'line',
      data: {        
        datasets: [{
            label: 'Chaina',
            data: this.datatest,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],          
            borderWidth: 3
        },
        ],
              labels: ['2015', '2016', '2017', '2018', '2019', '2020'],
          },
      options: {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero: false
                  }
              }]
          }
      }
    });
  
  }

}
