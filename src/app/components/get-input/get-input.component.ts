import { Component, OnInit } from '@angular/core';
import equationdata from '../../../assets/final_solution.json';


@Component({
  selector: 'app-get-input',
  templateUrl: './get-input.component.html',
  styleUrls: ['./get-input.component.scss']
})
export class GetInputComponent implements OnInit {

  private option;
  private money_unit:any;

  public oilpice:number;
  public rubber:number;
  public amount:number;
  public selectedRegion:any;
  public unit:string='';
  public predicted_value:number;
  constructor() { }

  ngOnInit() {
    this.selectedRegion = 'usa';
    this.oilpice = 0.00;
    this.rubber = 0.00;
    this.amount = 0.00;
    this.predicted_value = 0.00;
    this.money_unit = {'china':'หยวน','usa':'ดอลลาร์','japan':'เยน','malaysia':'ริงกิต'};
    this.unit = this.money_unit[this.selectedRegion];

  }

  getDataform(){
    if(this.oilpice < 0 || this.amount < 0 || this.rubber < 0 ){
      alert("กรอกค่าไม่ติดลบ");
    }
    var coef = equationdata[this.selectedRegion]['coef'];
    var intercept = equationdata[this.selectedRegion]['intercept'];

    this.predicted_value = (coef[0]*this.oilpice)+(coef[1]*this.rubber)+(coef[2]*this.amount)+intercept;
    // console.log(this.predicted_value);
  }

}
