import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BoardComponent } from './components/board/board.component';
import { GetInputComponent } from './components/get-input/get-input.component';
import { ChainaComponent } from './components/chaina/chaina.component';
import { UsaComponent } from './components/usa/usa.component';
import { JapanComponent } from './components/japan/japan.component';
import { MalaysiaComponent } from './components/malaysia/malaysia.component';
import { SingaporeComponent } from './components/singapore/singapore.component';
import { HomeComponent } from './components/home/home.component';


const routes: Routes = [
  { path: '',   redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'ch', component: ChainaComponent },
  { path: 'usa', component: UsaComponent },
  { path: 'jpn', component: JapanComponent },
  { path: 'mls', component: MalaysiaComponent },
  { path: 'sgp', component: SingaporeComponent },
  { path: 'getInput', component: GetInputComponent },
  // { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
