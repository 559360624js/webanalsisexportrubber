import { ChangeDetectionStrategy, Component } from '@angular/core';
import { NbIconConfig } from '@nebular/theme';
import { NbSidebarService } from '@nebular/theme';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'webDataAnalysis';

  items = [
    {
      title: 'หน้าหลัก',
      icon: 'home-outline',
      link: ['home'],
    },
    {
      title: 'ข้อมูลประเทศ',
      icon: 'pantone-outline',
      link: ['board'],
      expanded: true,
      children: [
        {
          title: 'จีน',
          link: ['ch'], // goes into angular `routerLink`
        },
        {
          title: 'อเมริกา',
          link: ['usa'], // goes directly into `href` attribute
        },
        {
          title: 'ญี่ปุ่น',
          link: ['jpn'],
        },
        {
          title: 'มาเลเซีย',
          link: ['mls'],
        },
      ],
    },
    {
      title: 'วิเคราะห์และคาดการณ์',
      icon: 'edit-2-outline',
      link: ['getInput'],
    },
    // {
    //   title: 'ตั้งค่า',
    //   icon: { icon: 'settings-outline', pack: 'eva' },
    //   link: [],
    // },
    // {
    //   title: 'ออกจากระบบ',
    //   icon: 'unlock-outline',
    //   link: [],
    // },
  ];

  constructor(private sidebarService: NbSidebarService) {
  }

  toggle() {
    this.sidebarService.toggle(true, 'left');
  }

}
